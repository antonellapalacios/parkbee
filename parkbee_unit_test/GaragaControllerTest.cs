using Microsoft.AspNetCore.Mvc;
using Moq;
using parkbee_asignment.Controllers;
using parkbee_asignment.Models;
using parkbee_asignment.Services.IServices;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Sdk;

namespace parkbee_unit_test
{
    public class GarageControllerTest
    {
        private IEnumerable<GarageViewModel> garagesMock;
        public GarageControllerTest()
        {
            garagesMock = new List<GarageViewModel> {
                new GarageViewModel { GarageId = 1, GarageName = "Test garage 1", NumberOfSpaces = 20 },
                new GarageViewModel { GarageId = 2, GarageName = "Test garage 2", NumberOfSpaces = 5 },
                new GarageViewModel { GarageId = 3, GarageName = "Test garage 3", NumberOfSpaces = 1 }
            }.AsEnumerable();
        }

        [Fact]
        public async Task GetAllGoodResponseTest()
        {
            var serviceMock = new Mock<IGarageService>();
            serviceMock.Setup(x => x.GetAll()).Returns(() => Task.FromResult(garagesMock));
            var controller = new GaragesController(serviceMock.Object);

            var items = await controller.Get();
            Assert.Equal(3, items.Count());
        }

        [Fact]
        public async Task GetAllFailledTest()
        {
            var serviceMock = new Mock<IGarageService>();
            serviceMock.Setup(x => x.GetAll()).Returns(() => Task.FromResult(garagesMock));
            var controller = new GaragesController(serviceMock.Object);
            var items = await controller.Get();
            var ex = Assert.Throws<AllException>(() => Assert.All(items, x => Assert.Equal(2, items.Count())));
            Assert.Equal(3, ex.Failures.Count);
            Assert.All(ex.Failures, x => Assert.IsType<EqualException>(x));
        }

        [Fact]
        public async Task GetByIdBadResponseTest()
        {
            var serviceMock = new Mock<IGarageService>();
            serviceMock.Setup(x => x.GetById(12)).Returns(() => Task.FromResult(new GarageViewModel()));
            var controller = new GaragesController(serviceMock.Object);
            ActionResult result = await controller.GetById(12);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task GetByIdGoodResponseTest()
        {
            var serviceMock = new Mock<IGarageService>();
            serviceMock.Setup(x => x.GetById(12)).Returns(() => Task.FromResult(new GarageViewModel { GarageId = 12 }));
            var controller = new GaragesController(serviceMock.Object);
            ActionResult result = await controller.GetById(12);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task UpdateBadResponseTest()
        {
            var serviceMock = new Mock<IGarageService>();
            GarageUpdateBindingModel garage = new GarageUpdateBindingModel
            {
                GarageId = 1,
                GarageName = "TEST",
                NumberOfSpaces = 1
            };

            serviceMock.Setup(x => x.Update(garage)).Returns(() => Task.FromResult("test"));
            var controller = new GaragesController(serviceMock.Object);
            ActionResult result = await controller.Update(garage);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task UpdateGoodResponseTest()
        {
            var serviceMock = new Mock<IGarageService>();
            GarageUpdateBindingModel garage = new GarageUpdateBindingModel
            {
                GarageId = 1,
                GarageName = "TEST",
                NumberOfSpaces = 1
            };

            serviceMock.Setup(x => x.Update(garage)).Returns(() => Task.FromResult(""));
            var controller = new GaragesController(serviceMock.Object);
            ActionResult result = await controller.Update(garage);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async Task AddDoorBadResponseTest()
        {
            var serviceMock = new Mock<IGarageService>();
            DoorBindingModel door = new DoorBindingModel
            {
                GarageId = 1,
                IpAddress = "TEST",
                Name = "TEST"
            };

            serviceMock.Setup(x => x.AddDoor(door)).Returns(() => Task.FromResult("test"));
            var controller = new GaragesController(serviceMock.Object);
            ActionResult result = await controller.AddDoorToGarage(door);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task AddDoorGoodResponseTest()
        {
            var serviceMock = new Mock<IGarageService>();
            DoorBindingModel door = new DoorBindingModel
            {
                GarageId = 1,
                IpAddress = "TEST",
                Name = "TEST"
            };

            serviceMock.Setup(x => x.AddDoor(door)).Returns(() => Task.FromResult(""));
            var controller = new GaragesController(serviceMock.Object);
            ActionResult result = await controller.AddDoorToGarage(door);
            Assert.IsType<OkResult>(result);
        }
    }
}
