﻿using Microsoft.EntityFrameworkCore;
using parkbee_asignment.Persistence.DataModels;
using parkbee_asignment.Persistence.Repositories.IRepositories;
using parkbee_asignment.Repositories;
using parkbeeasignment.DatabaseContexts;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace parkbee_unit_test
{
    public class GarageRepositoryTest
    {
        [Fact]
        public async Task CreateGarageOkTest()
        {
            IGarageRepository garageRepository = GetInMemoryGarageRepository();
            Garage garage = new Garage()
            {
                GarageName = "TEST",
                NumberOfSpaces = 10
            };

            var result = garageRepository.Create(garage);
            var items = await garageRepository.GetAll();
            Assert.Single(items);
            Assert.Equal("TEST", garage.GarageName);
            Assert.Equal(10, garage.NumberOfSpaces);
            Assert.Empty(garage.Doors);
        }

        [Fact]
        public async Task UpdateGarageOkTest()
        {
            IGarageRepository garageRepository = GetInMemoryGarageRepository();
            Garage garage = new Garage()
            {
                GarageName = "TEST",
                NumberOfSpaces = 10,
                GarageId = 1
            };

            await garageRepository.Create(garage);
            garage.GarageName = "Updated";
            await garageRepository.Update(garage);
            var items = await garageRepository.GetAll();
            var updated = items.FirstOrDefault(x => x.GarageId == garage.GarageId);
            Assert.NotNull(updated);
            Assert.Equal(garage.GarageName, updated.GarageName);
            Assert.Equal(garage.NumberOfSpaces, updated.NumberOfSpaces);
        }

        [Fact]
        public async Task UpdateGarageNotExistTest()
        {
            IGarageRepository garageRepository = GetInMemoryGarageRepository();
            Garage garage = new Garage()
            {
                GarageName = "TEST",
                NumberOfSpaces = 10,
                GarageId = 1123
            };

            await Assert.ThrowsAsync<DbUpdateConcurrencyException>(() => garageRepository.Update(garage));
        }

        [Fact]
        public async Task GetAllGarageOkTest()
        {
            IGarageRepository garageRepository = GetInMemoryGarageRepository();
            await garageRepository.Create(new Garage { GarageName = "TEST", NumberOfSpaces = 10 });
            await garageRepository.Create(new Garage { GarageName = "TEST2", NumberOfSpaces = 102 });

            var items = await garageRepository.GetAll();
            Assert.Equal(2, items.Count());
        }

        [Fact]
        public async Task GetByIdNotExistTest()
        {
            int id = 12;
            IGarageRepository garageRepository = GetInMemoryGarageRepository();
            await garageRepository.Create(new Garage { GarageName = "TEST", NumberOfSpaces = 10 });
            await garageRepository.Create(new Garage { GarageName = "TEST2", NumberOfSpaces = 102 });

            var result = await garageRepository.GetById(id);
            Assert.Null(result);
        }


        [Fact]
        public async Task GetByIdOkTest()
        {
            IGarageRepository garageRepository = GetInMemoryGarageRepository();
            await garageRepository.Create(new Garage { GarageName = "TEST", NumberOfSpaces = 10 });
            await garageRepository.Create(new Garage { GarageName = "TEST2", NumberOfSpaces = 102 });
            var all = await garageRepository.GetAll();
            var result = await garageRepository.GetById(all.First().GarageId);
            Assert.Equal(all.First().GarageId, result.GarageId);
        }

        [Fact]
        public async Task NotExistTest()
        {
            int id = 12;
            IGarageRepository garageRepository = GetInMemoryGarageRepository();
            await garageRepository.Create(new Garage { GarageName = "TEST", NumberOfSpaces = 10 });
            await garageRepository.Create(new Garage { GarageName = "TEST2", NumberOfSpaces = 102 });

            var result = await garageRepository.Exists(id);
            Assert.False(result);
        }

        [Fact]
        public async Task ExistsOkTest()
        {           
            IGarageRepository garageRepository = GetInMemoryGarageRepository();
            await garageRepository.Create(new Garage { GarageName = "TEST", NumberOfSpaces = 10 });
            await garageRepository.Create(new Garage { GarageName = "TEST2", NumberOfSpaces = 102 });
            var all = await garageRepository.GetAll();
            var result = await garageRepository.Exists(all.First().GarageId);
            Assert.True(result);
        }

        [Fact]
        public async Task AddDoorGarageNotExistsTest()
        {
            var door = new Door
            {
                GarageId = 12,
                IpAddress = "address",
                Name = "DOOR TEST",
            };

            IGarageRepository garageRepository = GetInMemoryGarageRepository();
            var result = await garageRepository.GetAll();
            Assert.Empty(result.Where(x => x.Doors != null));
        }

        [Fact]
        public async Task AddDoorOkTest()
        {
            IGarageRepository garageRepository = GetInMemoryGarageRepository();
            await garageRepository.Create(new Garage { GarageName = "TEST", NumberOfSpaces = 10 });
            await garageRepository.Create(new Garage { GarageName = "TEST2", NumberOfSpaces = 102 });
            var items = await garageRepository.GetAll();
            int id = items.First().GarageId;
            var door = new Door
            {
                GarageId = id,
                IpAddress = "address",
                Name = "DOOR TEST",
            };
            await garageRepository.AddDoor(door);
            var garage = await garageRepository.GetById(door.GarageId);
            Assert.NotNull(garage);
            Assert.Single(garage.Doors);
            Assert.Equal(garage.Doors.First().Name, door.Name);
            Assert.Equal(garage.Doors.First().IpAddress, door.IpAddress);
        }
        private IGarageRepository GetInMemoryGarageRepository()
        {
            DbContextOptions<ParkbeeContext> options;
            var builder = new DbContextOptionsBuilder<ParkbeeContext>();
            builder.UseInMemoryDatabase("parkbee");
            options = builder.Options;
            ParkbeeContext personDataContext = new ParkbeeContext(options);
            personDataContext.Database.EnsureDeleted();
            personDataContext.Database.EnsureCreated();
            return new GarageRepository(personDataContext);
        }
    }
}
