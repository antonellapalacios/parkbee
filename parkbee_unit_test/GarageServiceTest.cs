﻿using Moq;
using parkbee_asignment.Models;
using parkbee_asignment.Persistence.DataModels;
using parkbee_asignment.Persistence.Repositories.IRepositories;
using parkbee_asignment.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Sdk;

namespace parkbee_unit_test
{
    public class GarageServiceTest
    {
        [Fact]
        public async Task GetAllCountTest()
        {
            var repositoryMock = new Mock<IGarageRepository>();
            repositoryMock.Setup(x => x.GetAll()).Returns(() => Task.FromResult(
               new List<Garage>
               {
                   new Garage{GarageId = 1, GarageName = "TEST 1", NumberOfSpaces = 1},
                   new Garage {GarageId = 2, GarageName = "TEST 2", NumberOfSpaces = 2}
               }.AsEnumerable()));
            var service = new GarageService(repositoryMock.Object);

            var items = await service.GetAll();
            Assert.Equal(2, items.Count());
        }

        [Fact]
        public async Task GetAllCountBadTest()
        {
            var repositoryMock = new Mock<IGarageRepository>();
            repositoryMock.Setup(x => x.GetAll()).Returns(() => Task.FromResult(
               new List<Garage>
               {
                   new Garage{GarageId = 1, GarageName = "TEST 1", NumberOfSpaces = 1},
                   new Garage {GarageId = 2, GarageName = "TEST 2", NumberOfSpaces = 2}
               }.AsEnumerable()));
            var service = new GarageService(repositoryMock.Object);

            var items = await service.GetAll();
            var ex = Assert.Throws<AllException>(() => Assert.All(items, x => Assert.Equal(3, items.Count())));
            Assert.Equal(2, ex.Failures.Count);
            Assert.All(ex.Failures, x => Assert.IsType<EqualException>(x));
        }
        
        [Fact]
        public async Task GetByIdNotExistsTest()
        {
            var repositoryMock = new Mock<IGarageRepository>();
            int id = 1;
            repositoryMock.Setup(x => x.GetById(id)).Returns(() => Task.FromResult(new Garage()));
            var service = new GarageService(repositoryMock.Object);
            var result = await service.GetById(id);
            Assert.IsType<GarageViewModel>(result);
            Assert.Equal(0, result.GarageId);
        }

        [Fact]
        public async Task GetByIdOkTest()
        {
            var repositoryMock = new Mock<IGarageRepository>();
            int id = 1;
            repositoryMock.Setup(x => x.GetById(id)).Returns(() => Task.FromResult(
                   new Garage { GarageId = id, GarageName = "TEST 1", NumberOfSpaces = 1 }));
            var service = new GarageService(repositoryMock.Object);

            var result = await service.GetById(id);
            Assert.IsType<GarageViewModel>(result);
            Assert.Equal(id, result.GarageId);
        }

        [Fact]
        public async Task UpdateNotExistTest()
        {
            var repositoryMock = new Mock<IGarageRepository>();
            var garageBin = new GarageUpdateBindingModel
            {
                GarageId = 1,
                NumberOfSpaces = 10,
                GarageName = "TEST"
            };

            repositoryMock.Setup(x => x.Exists(garageBin.GarageId)).Returns(() => Task.FromResult(false));

            var service = new GarageService(repositoryMock.Object);
            var result = await service.Update(garageBin);
            Assert.NotNull(result);
        }
        
        [Fact]
        public async Task UpdateOKTest()
        {
            var repositoryMock = new Mock<IGarageRepository>();
            var garage = new Garage
            {
                GarageId = 1,
                NumberOfSpaces = 10,
                GarageName = "TEST"
            };
            var garageBin = new GarageUpdateBindingModel
            {
                GarageId = 1,
                NumberOfSpaces = 10,
                GarageName = "TEST"
            };

            repositoryMock.Setup(x => x.Exists(garage.GarageId)).Returns(() => Task.FromResult(true));
            repositoryMock.Setup(x => x.Update(garage)).Returns(() => null);

            var service = new GarageService(repositoryMock.Object);
            var result = await service.Update(garageBin);
            Assert.Null(result);
        }

        [Fact]
        public async Task AddDoorOKTest()
        {
            var repositoryMock = new Mock<IGarageRepository>();
            Door door = new Door
            {
                GarageId = 1,
                IpAddress = "TEST",
                Name = "TEST"
            };
            var garage = new Garage
            {
                GarageId = 1,
                NumberOfSpaces = 10,
                GarageName = "TEST",
                Doors = new List<Door>()
                    {
                        new Door { DoorId = 1, GarageId = 1},
                        new Door { DoorId = 2, GarageId = 1},
                    }
            };

            repositoryMock.Setup(x => x.GetById(1)).Returns(() => Task.FromResult(garage));
            repositoryMock.Setup(x => x.AddDoor(door)).Returns(() => null);

            var service = new GarageService(repositoryMock.Object);
            var result = await service.AddDoor(new DoorBindingModel { GarageId = 1 });
            Assert.Null(result);
        }

        [Fact]
        public async Task AddDoorGarageIdNotExistTest()
        {
            var repositoryMock = new Mock<IGarageRepository>();
            
            repositoryMock.Setup(x => x.GetById(1)).Returns(() => null);

            var service = new GarageService(repositoryMock.Object);
            var door = new DoorBindingModel { GarageId = 2 };
            var result = await service.AddDoor(door);
            Assert.Equal("The garage " + door.GarageId + " does not exist.", result);
        }
        
        [Fact]
        public async Task AddDoorReachedMaxTest()
        {
            var repositoryMock = new Mock<IGarageRepository>();
            Door door = new Door
            {
                GarageId = 1,
                IpAddress = "TEST",
                Name = "TEST"
            };
            var garage = new Garage
            {
                GarageId = 1,
                NumberOfSpaces = 10,
                GarageName = "TEST",
                Doors = new List<Door>()
                    {
                        new Door { DoorId = 1, GarageId = 1},
                        new Door { DoorId = 2, GarageId = 1},
                        new Door { DoorId = 3, GarageId = 1}
                    }
            };

            repositoryMock.Setup(x => x.GetById(1)).Returns(() => Task.FromResult(garage));

            var service = new GarageService(repositoryMock.Object);
            var result = await service.AddDoor(new DoorBindingModel { GarageId = 1 });
            Assert.Equal("The garage " + garage.GarageId + " reached the maximum number of doors, It's not possible to add a new one.", result);
        }
    }
}
