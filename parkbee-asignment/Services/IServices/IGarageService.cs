﻿using parkbee_asignment.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace parkbee_asignment.Services.IServices
{
    public interface IGarageService
    {
        Task Create(GarageCreateBindingModel garage);
        Task<IEnumerable<GarageViewModel>> GetAll();
        Task<string> Update(GarageUpdateBindingModel garage);
        Task<GarageViewModel> GetById(int id);
        Task<string> AddDoor(DoorBindingModel door);
    }
}
