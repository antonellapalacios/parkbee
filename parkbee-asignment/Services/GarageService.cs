﻿using parkbee_asignment.Extensions;
using parkbee_asignment.Models;
using parkbee_asignment.Persistence.DataModels;
using parkbee_asignment.Persistence.Repositories.IRepositories;
using parkbee_asignment.Services.IServices;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace parkbee_asignment.Services
{
    public class GarageService : IGarageService
    {
        const int MAX_NUMBERS_OF_DOOR = 3;
        private readonly IGarageRepository garageRepository;
        public GarageService(IGarageRepository garageRepository)
        {
            this.garageRepository = garageRepository;
        }

        public async Task Create(GarageCreateBindingModel garage)
        {
            await garageRepository.Create(garage.MapToEntity());
        }

        public async Task<IEnumerable<GarageViewModel>> GetAll()
        {
            var result = await garageRepository.GetAll();
            return result.Select(x => x.MapToViewModel());
        }

        public async Task<GarageViewModel> GetById(int id)
        {
            var result = await garageRepository.GetById(id);
            return result.MapToViewModel();
        }

        public async Task<string> Update(GarageUpdateBindingModel garage)
        {
            if (await garageRepository.Exists(garage.GarageId))
            {
                await garageRepository.Update(garage.MapToEntity());
                return null;
            }
            else
            {
                return "The garage " + garage.GarageId + " does not exist.";
            }
        }

        public async Task<string> AddDoor(DoorBindingModel door)
        {
            Garage garage = await garageRepository.GetById(door.GarageId);
            if(garage == null)
            {
                return "The garage " + door.GarageId + " does not exist.";
            }

            if (garage.Doors != null && garage.Doors.Count() >= MAX_NUMBERS_OF_DOOR)
            {
                return "The garage " + garage.GarageId + " reached the maximum number of doors, It's not possible to add a new one.";
            }

            await garageRepository.AddDoor(door.MapToEntiy());
            return null;
        }
    }
}
