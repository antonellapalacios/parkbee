# Assignment medior developer


Your job is to finish the API implementation according to the following specs : 

1.  Finish the implementation of the existing api methods that :  
    - creates a garage and persists it to the in memory database
    - gets the garages from the in memory database

	constraints : 
2. Add api methods to :
    - update a garage 
    - add a door to a garage
    - return a single garage by id

    constraints : 
    - A door model consists of a name and an ip address
    - It should be enforced that you can add a maximum of 3 doors to a garage.


Ensure that the logic is covered by unit tests

This API is already setup with 1 controller containing two methods which are returning static data. 
There is a DB Context configured which can be used as an in memory database, this is where the data needs to be persisted when running the API.
Interacting with the API can be done using Swagger, this is also configured.
When you start the application and go to http://localhost:5000/swagger you will see the swagger page.

Note : Consider using dependency injection, and adding logic to a separate class instead of in the controller

Extra : 	- Make the api methods asynchronous 
		- Ensure that swagger is returning the correct response codes and types
   	