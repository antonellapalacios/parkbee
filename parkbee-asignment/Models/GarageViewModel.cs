﻿using System.Collections.Generic;

namespace parkbee_asignment.Models
{
    public class GarageViewModel
    {
        public int GarageId { get; set; }
        public string GarageName { get; set; }
        public int NumberOfSpaces { get; set; }
        public IEnumerable<DoorViewModel> Doors { get; set; }
    }
}
