﻿using System.ComponentModel.DataAnnotations;

namespace parkbee_asignment.Models
{
    public class GarageCreateBindingModel
    {
        [Required]
        public string GarageName { get; set; }
        [Required]
        public int NumberOfSpaces { get; set; }
    }
}
