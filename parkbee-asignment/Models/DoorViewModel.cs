﻿namespace parkbee_asignment.Models
{
    public class DoorViewModel
    {
        public int DoorId { get; set; }
        public string Name { get; set; }
        public string IpAddress { get; set; }
    }
}
