﻿using System.ComponentModel.DataAnnotations;

namespace parkbee_asignment.Models
{
    public class GarageUpdateBindingModel
    {
        [Required]
        public int GarageId { get; set; }
        [Required]
        public string GarageName { get; set; }
        [Required]
        public int NumberOfSpaces { get; set; }
    }
}
