﻿using System.ComponentModel.DataAnnotations;

namespace parkbee_asignment.Models
{
    public class DoorBindingModel
    {
        [Required]
        public string Name { get; set; }
        public string IpAddress { get; set; }
        public int GarageId { get; set; }
    }
}
