﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using parkbee_asignment.Persistence.Repositories.IRepositories;
using parkbee_asignment.Repositories;
using parkbee_asignment.Services;
using parkbee_asignment.Services.IServices;
using parkbeeasignment.DatabaseContexts;

namespace parkbee_asignment
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info { Title = "Docs", Version = "v1" });
            });
            services.AddDbContext<ParkbeeContext>(options => options.UseInMemoryDatabase("parkbee"));
            services.AddTransient<IGarageService, GarageService>();
            services.AddTransient<IGarageRepository, GarageRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.InjectStylesheet("/swagger-ui/custom.css");
                c.DocumentTitle = "Something something";
            });

            app.UseMvc();
        }
    }
}
