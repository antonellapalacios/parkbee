﻿using parkbee_asignment.Models;
using parkbee_asignment.Persistence.DataModels;

namespace parkbee_asignment.Extensions
{
    public static class DoorExtensions
    {
        public static Door MapToEntiy(this DoorBindingModel door)
        {
            if(door == null)
            {
                return new Door();
            }

            return new Door
            {
                IpAddress = door.IpAddress,
                Name = door.Name,
                GarageId = door.GarageId
            };
        }

        public static DoorViewModel MapToViewModel(this Door door)
        {
            if(door == null)
            {
                return new DoorViewModel();
            }

            return new DoorViewModel
            {
                DoorId = door.DoorId,
                IpAddress = door.IpAddress,
                Name = door.Name
            };
        }
    }
}
