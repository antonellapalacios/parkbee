﻿using parkbee_asignment.Models;
using parkbee_asignment.Persistence.DataModels;
using System.Linq;

namespace parkbee_asignment.Extensions
{
    public static class GarageExtensions
    {
        public static Garage MapToEntity(this GarageCreateBindingModel garage)
        {
            if(garage == null)
            {
                return new Garage();
            }

            return new Garage
            {
                GarageName = garage.GarageName,
                NumberOfSpaces = garage.NumberOfSpaces
            };
        }

        public static Garage MapToEntity(this GarageUpdateBindingModel garage)
        {
            if(garage == null)
            {
                return new Garage();
            }

            return new Garage
            {
                GarageId = garage.GarageId,
                GarageName = garage.GarageName,
                NumberOfSpaces = garage.NumberOfSpaces
            };
        }

        public static GarageViewModel MapToViewModel(this Garage garage)
        {
            if(garage == null)
            {
                return new GarageViewModel();
            }

            return new GarageViewModel
            {
                GarageName = garage.GarageName,
                NumberOfSpaces = garage.NumberOfSpaces,
                GarageId = garage.GarageId,
                Doors = garage.Doors != null ? garage.Doors.Select(x => x.MapToViewModel()) : null
            };
        }
    }
}
