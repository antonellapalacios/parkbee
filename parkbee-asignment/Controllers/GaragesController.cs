﻿using Microsoft.AspNetCore.Mvc;
using parkbee_asignment.Models;
using parkbee_asignment.Services.IServices;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace parkbee_asignment.Controllers
{
    [Route("api/[controller]/[action]")]
    public class GaragesController : Controller
    {
        private readonly IGarageService garageService;
        public GaragesController(IGarageService garageService)
        {
            this.garageService = garageService;
        }

        [HttpGet]
        [ActionName("Get")]
        public async Task<IEnumerable<GarageViewModel>> Get()
        {
            return await garageService.GetAll();         
        }

        [HttpPost]
        [ActionName("Create")]
        public async Task<ActionResult> Create(GarageCreateBindingModel garage)
        {
            await garageService.Create(garage);
            return new OkResult();
        }

        [HttpPost]
        [ActionName("Update")]
        public async Task<ActionResult> Update(GarageUpdateBindingModel garage)
        {
            var result = await garageService.Update(garage);
            if (!string.IsNullOrEmpty(result))
            {
                return new BadRequestObjectResult(result);
            }

            return new OkResult();
        }

        [HttpGet]
        [ActionName("GetById")]
        public async Task<ActionResult> GetById(int id)
        {
            var result = await garageService.GetById(id);
            if(result == null || result.GarageId == 0)
            {
                return new BadRequestObjectResult("There is not a garage for the id " + id.ToString());
            }

            return new OkObjectResult(result);
        }

        [HttpPost]
        [ActionName("AddDoorToGarage")]
        public async Task<ActionResult> AddDoorToGarage(DoorBindingModel door)
        {
            var result = await garageService.AddDoor(door);
            if(!string.IsNullOrEmpty(result))
            {
                return new BadRequestObjectResult(result);
            }

            return new OkResult();
        }
    }
}
