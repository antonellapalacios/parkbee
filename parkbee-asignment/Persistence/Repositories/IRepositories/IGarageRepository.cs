﻿using parkbee_asignment.Persistence.DataModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace parkbee_asignment.Persistence.Repositories.IRepositories
{
    public interface IGarageRepository 
    {
        Task<Garage> GetById(int id);
        Task<bool> Exists(int id);
        Task AddDoor(Door door);
        Task Create(Garage garage);
        Task<IEnumerable<Garage>> GetAll();
        Task Update(Garage garage);
    }
}
