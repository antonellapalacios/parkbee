﻿using Microsoft.EntityFrameworkCore;
using parkbee_asignment.Persistence.DataModels;
using parkbee_asignment.Persistence.Repositories.IRepositories;
using parkbeeasignment.DatabaseContexts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace parkbee_asignment.Repositories
{
    public class GarageRepository : IGarageRepository
    {
        private ParkbeeContext context;
        public GarageRepository(ParkbeeContext context)
        {
            this.context = context;
        }

        public async Task Create(Garage garage)
        {
            if (garage == null)
            {
                throw new ArgumentNullException("entity");
            }
            await context.AddAsync(garage);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Garage>> GetAll()
        {
            return await context.garages.Include(garage => garage.Doors).ToListAsync();
        }

        public async Task Update(Garage garage)
        {
            context.Update(garage);
            await context.SaveChangesAsync();
        }

        public async Task<Garage> GetById(int id)
        {
            return await context.garages.Include(garage => garage.Doors).FirstOrDefaultAsync(x => x.GarageId == id); 
        }

        public async Task<bool> Exists(int id)
        {
            return await context.garages.AnyAsync(x => x.GarageId == id);
        }

        public async Task AddDoor(Door door)
        {
            await context.doors.AddAsync(door);
            await context.SaveChangesAsync();
        }
    }
}
