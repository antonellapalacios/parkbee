﻿using Microsoft.EntityFrameworkCore;
using parkbee_asignment.Persistence.DataModels;

namespace parkbeeasignment.DatabaseContexts
{
    public class ParkbeeContext : DbContext
    {
        public ParkbeeContext(DbContextOptions<ParkbeeContext> options)
      : base(options)
        { }

        public DbSet<Garage> garages { get; set; }
        public DbSet<Door> doors { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0");
            }
        }
    }
}
