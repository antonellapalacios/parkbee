﻿using System.Collections.Generic;

namespace parkbee_asignment.Persistence.DataModels
{
    public class Garage
    {
        public Garage()
        {
        }

        public int GarageId { get; set; }
        public string GarageName { get; set; }
        public int NumberOfSpaces { get; set; }
        public IEnumerable<Door> Doors { get; set; }
    }
}
