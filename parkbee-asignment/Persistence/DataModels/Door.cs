﻿namespace parkbee_asignment.Persistence.DataModels
{
    public class Door
    {
        public int DoorId { get; set; }
        public string Name { get; set; }
        public string IpAddress { get; set; }
        public int GarageId { get; set; }
        public Garage Garage { get; set; }
    }
}
